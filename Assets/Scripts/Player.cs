﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour {
    [SerializeField] private int _movementSpeed = 200;
    [SerializeField] private Vector3 _velocity;
    [SerializeField] private Vector3 _addedForce;
    [SerializeField] private SpriteRenderer _weapon;
    private Rigidbody _rb;
    private InputMaster _controls;
    private float _maxSpeed = 4;
    private Vector3 _turnDir;
    private float _addedSpeed = 1;
    private Animator _anim;
    private Interactable _interactable;
    private LootableItem _lootable;

    public Interactable Interactable {
        set {
            _interactable = value;
        }
    }
    public LootableItem Lootable {
        set {
            _lootable = value;
        }
    }

    public void AddForce (Vector3 add) {
        _addedForce = add;
    }

    void Awake () {
        _anim = GetComponent<Animator> ();
        _rb = GetComponent<Rigidbody> ();
        _controls = new InputMaster ();
        _controls.Player.Movement.performed += ctx => Move (ctx.ReadValue<Vector2> ());
        _controls.Player.Movement.canceled += ctx => Move (ctx.ReadValue<Vector2> ());
        _controls.Player.Interact.performed += ctx => Interact ();
        _controls.Player.Next.performed += ctx => DialogueSystem.Instance.Next ();
    }

    void OnEnable () {
        _controls.Enable ();
    }

    void OnDisable () {
        _controls.Disable ();
    }

    public void FixedUpdate () {
        _turnDir = Statics.GetMouseDir (transform.position);
        if (_turnDir.x >= -0.3f && _turnDir.x <= 0.3f)
            return;

        // if (Statics.GetTurn (_turnDir.x)) {
        //     _spine.rotation = Quaternion.Euler (new Vector3 (0, 180, 0));
        // } else {
        //     _spine.rotation = new Quaternion (0, 0, 0, 0);
        // }

        _rb.velocity = _velocity;
        if (_velocity.x != 0 || _velocity.z != 0) {
            _anim.SetFloat ("Speed", GetAnimSpeed (_velocity.x, _turnDir.x));
        } else {
            _anim.SetFloat ("Speed", 0);
        }
    }

    void Interact () {
        if (_interactable != null) {
            _interactable.Interact (gameObject);
        }
        if (_lootable != null) {
            _lootable.LootItem ();
        }
    }

    public void EquipWeapon (Sprite sprite) {
        _weapon.sprite = sprite;
    }

    public void Move (Vector3 dir) {
        dir.z = dir.y;
        dir.y = 0;
        SetRBVelocity ((dir * _movementSpeed * Time.deltaTime) * _addedSpeed);
    }
    private void SetRBVelocity (Vector3 velo) {
        _velocity.x = Mathf.Clamp (velo.x, -_maxSpeed, _maxSpeed);
        _velocity.z = Mathf.Clamp (velo.z, -_maxSpeed, _maxSpeed);
    }

    float GetAnimSpeed (float x1, float x2) => (x1 < 0f && x2 > 0) || (x1 > 0 && x2 < 0) ? -1 : 1;
}