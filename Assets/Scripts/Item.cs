using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ItemType {
    Weapon,
    Key
}

[Serializable]
[CreateAssetMenu (fileName = "New Item", menuName = "New Item")]
public class Item : ScriptableObject {
    public string itemName = "Set Name";
    public ItemType itemType;
    public Sprite sprite;
    public Mesh model;
}