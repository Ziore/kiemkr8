﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private int _stage = 0;

    public int Stage {
        get {
            return _stage;
        }
    }

    private static GameManager _manager;
    public static GameManager Instance {
        get {
            if (_manager == null) _manager = FindObjectOfType<GameManager> ();
            return _manager;
        }
    }

    public void GotoScene (int i) {
        SceneManager.LoadScene (i);
    }

    public void AdvanceStage () {
        _stage++;
        Tasks.Instance.OnCompleteTask();
    }
}