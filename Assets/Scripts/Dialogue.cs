using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class Dialogue : ScriptableObject {
    [SerializeField] public int amount;
    [SerializeField] public List<string> npcName = new List<string>();
    [SerializeField] public List<string> textpool = new List<string>();
    [SerializeField] public List<Sprite> imagepool = new List<Sprite>();
    [SerializeField] internal List<bool> showInInspector = new List<bool>();
    public bool dialogueFinished = false;

    public bool Get (int textCounter, Text npcText, Text dialogText, Image sprite) //returns false if dialog is finished
    {
        if (textpool.Count > textCounter) {
            npcText.text = npcName[textCounter];
            dialogText.text = textpool[textCounter];
            sprite.sprite = imagepool[textCounter];
            return true;
        }
        return false;
    }
}
#if UNITY_EDITOR
[CustomEditor (typeof (Dialogue))]
public class DialogueEditor : Editor {
    private Dialogue dialogue { get { return (target as Dialogue); } }
    private bool showValues = true;

    void OnEnable () {
        if (dialogue.amount == dialogue.npcName.Count)
            return;
        UpdateAmount ();
    }

    void UpdateAmount () {
        if (dialogue.amount > dialogue.npcName.Count) {
            dialogue.npcName.Add ("");
            dialogue.textpool.Add ("");
            dialogue.imagepool.Add (null);
            dialogue.showInInspector.Add (false);
        } else {
            int from = dialogue.amount;
            int amount = dialogue.npcName.Count - from;

            dialogue.npcName.RemoveRange (from, amount);
            dialogue.textpool.RemoveRange (from, amount);
            dialogue.imagepool.RemoveRange (from, amount);
            dialogue.showInInspector.RemoveRange (from, amount);
        }
    }

    public override void OnInspectorGUI () {
        EditorGUI.BeginChangeCheck ();
        dialogue.amount = EditorGUILayout.IntField ("Amount Of Dialogues", dialogue.amount);
        EditorGUILayout.Space ();

        if (dialogue.amount != dialogue.npcName.Count) {
            UpdateAmount ();
        }

        showValues = EditorGUILayout.Foldout (showValues, "Dialogue : ");
        if (showValues) {
            for (int i = 0; i < dialogue.amount; i++) {
                EditorGUI.indentLevel++;
                dialogue.showInInspector[i] = EditorGUILayout.Foldout (dialogue.showInInspector[i], "Phrase - " + i);
                if (dialogue.showInInspector[i]) {
                    dialogue.npcName[i] = EditorGUILayout.TextField ("NPC Name", dialogue.npcName[i]);
                    dialogue.textpool[i] = EditorGUILayout.TextField ("Phrase", dialogue.textpool[i]);
                    dialogue.imagepool[i] = (Sprite) EditorGUILayout.ObjectField ("Sprite ", dialogue.imagepool[i], typeof (Sprite), false, null);
                }

                EditorGUI.indentLevel--;
            }
        }

        if (EditorGUI.EndChangeCheck ()) {
            EditorUtility.SetDirty (dialogue);
            AssetDatabase.SaveAssets ();
            serializedObject.ApplyModifiedProperties ();
        }

    }
}
#endif