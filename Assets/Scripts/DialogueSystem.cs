using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour {
    private Dialogue dialogue;
    private int counter;
    [SerializeField] private Text npcName;
    [SerializeField] private Text dialogueText;
    private Image sprite;
    private GameObject dialogueVisuals;

    private static DialogueSystem _dialogueSystem;
    public static DialogueSystem Instance {
        get {
            if (_dialogueSystem == null) _dialogueSystem = FindObjectOfType<DialogueSystem> ();
            return _dialogueSystem;
        }
    }

    void Awake () {
        sprite = transform.GetChild (0).Find ("Icon").GetComponent<Image> ();
        dialogueVisuals = transform.GetChild (0).gameObject;
        dialogueVisuals.SetActive (false);
    }

    public void Open (Dialogue dialogue) {
        dialogueVisuals.SetActive (true);
        this.dialogue = dialogue;
        counter = 0;
        Next ();
    }

    internal void Next () {
        bool s = dialogue.Get (counter, npcName, dialogueText, sprite);
        if (!s) {
            dialogueVisuals.SetActive (false);
        }
        counter++;
    }
}