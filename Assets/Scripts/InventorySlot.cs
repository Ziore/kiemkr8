using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour {
    private Image _icon;
    private Inventory _inventory;
    private Item _item;

    public Item Item {
        get {
            return _item;
        }
    }

    void Awake () {
        _icon = transform.GetChild (0).GetComponent<Image> ();
        _inventory = Inventory.Instance;
    }
    public void SetItem (Item item) {
        if (item == null) {
            gameObject.SetActive (false);
            return;
        }
        gameObject.SetActive (true);
        _item = item;
        _icon.sprite = _item.sprite;
    }
}