using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DialogueTrigger : Trigger {
    [SerializeField] private Dialogue _dialogue;
    
    public bool dialogueFinished = false;

    protected override void Enter (Player p) {
        if(!dialogueFinished)
            DialogueSystem.Instance.Open (_dialogue);
        dialogueFinished = true;
    }
    protected override void Stay (Player p) { }

    protected override void Exit (Player p) { }
}