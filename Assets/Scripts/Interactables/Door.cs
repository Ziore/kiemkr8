using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Door : Interactable {
    [SerializeField] private int _toSceneNum = 0;
    [SerializeField] private Transform _canPos;

    public override void Interact (GameObject player) {
        if (GameManager.Instance.Stage < _toSceneNum) return;

        StartCoroutine (AnimateSceneSwitch ());
    }

    public IEnumerator AnimateSceneSwitch () {
        gameObject.layer = LayerMask.NameToLayer ("Postprocessing");
        GetComponentInChildren<Animator> ().SetTrigger ("Door");
        
        CameraFollow.Instance.SetPOI (_canPos);
        PostProcessVolume volume = GetComponent<PostProcessVolume> ();

        while (volume.weight < 1) {
            volume.weight += Time.deltaTime;
            yield return new WaitForSeconds (Time.deltaTime);
        }
        yield return new WaitForSeconds (1);
        GameManager.Instance.GotoScene (_toSceneNum);
    }
}