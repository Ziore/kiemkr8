﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : Trigger {

    [SerializeField] private Transform POIObject;
    [SerializeField] private Transform RoomObject;

    protected override void Enter (Player p) {
        CameraFollow.Instance.SetPOI (POIObject);
    }
    protected override void Stay (Player p) { }

    protected override void Exit (Player p) {
        CameraFollow.Instance.SetPOI (RoomObject);
     }
}