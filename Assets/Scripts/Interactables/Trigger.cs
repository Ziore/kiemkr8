using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent (typeof (BoxCollider))]
public abstract class Trigger : MonoBehaviour {
    void Awake () {
        GetComponent<BoxCollider> ().isTrigger = true;
    }

    void OnTriggerEnter (Collider other) {
        Player p = other.GetComponent<Player> ();
        if (p != null) {
            Enter (p);
        }
    }
    void OnTriggerStay (Collider other) {
        Player p = other.GetComponent<Player> ();
        if (p != null) {
            Stay (p);
        }
    }

    void OnTriggerExit (Collider other) {
        Player p = other.GetComponent<Player> ();
        if (p != null) {
            Exit (p);
        }
    }

    protected abstract void Enter (Player p);
    protected abstract void Stay (Player p);
    protected abstract void Exit (Player p);
}