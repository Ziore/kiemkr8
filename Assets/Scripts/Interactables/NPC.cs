﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPCDialogue {
	public NPC[] toUpdateNPCs;
	public Dialogue[] newPhrases;
	public bool advanceStage;
}
public class NPC : Interactable {
	public Dialogue Dialogue;
	public NPCDialogue[] ChangeNPCDialogue;
	private int nextDialogue = 0;
	private GameObject _textBox;

	void Awake () {
		_textBox = Instantiate (Resources.Load<GameObject> ("Prefabs/TextBox"), transform);
		_textBox.SetActive (false);
	}

	public override void Interact (GameObject player) {
		DialogueSystem.Instance.Open (Dialogue);

		if (ChangeNPCDialogue.Length > nextDialogue) {
			for (int i = 0; i < ChangeNPCDialogue[nextDialogue].toUpdateNPCs.Length; i++) {
				ChangeNPCDialogue[nextDialogue].toUpdateNPCs[i].Dialogue = ChangeNPCDialogue[nextDialogue].newPhrases[i];
				if (ChangeNPCDialogue[nextDialogue].advanceStage) {
					GameManager.Instance.AdvanceStage ();
				}
			}
		}
	}
	public override void EnterArea (GameObject player) {
		_textBox.SetActive (true);
	}
	public override void LeaveArea (GameObject player) {
		_textBox.SetActive (false);
	}
}