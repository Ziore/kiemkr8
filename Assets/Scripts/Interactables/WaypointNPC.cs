using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointNPC : NPC {
    [SerializeField] private Transform[] _waypoints;
    [SerializeField] private float _speed = 5;
    private int _currentWaypoint = 0;
    private bool _stopped = false;

    void Update () {
        if (!_stopped) {
            transform.position = Vector3.MoveTowards (transform.position, _waypoints[_currentWaypoint].position, _speed * Time.deltaTime);
            if (Vector3.Distance (transform.position, _waypoints[_currentWaypoint].position) < 2) {
                _currentWaypoint++;
                if (_currentWaypoint >= _waypoints.Length)
                    _currentWaypoint = 0;
            }
        }
    }

    public override void EnterArea (GameObject player) {
        base.EnterArea (player);
        _stopped = true;
    }
    public override void LeaveArea (GameObject player) {
        base.LeaveArea (player);
        _stopped = false;
    }
}