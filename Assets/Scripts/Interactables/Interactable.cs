﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider))]
public class Interactable : MonoBehaviour {
	[SerializeField] private string _text;
	void OnEnable () {
		gameObject.layer = LayerMask.NameToLayer ("Interactable");
	}

	public void OnTriggerEnter (Collider other) {
		Player player = other.GetComponent<Player> ();
		if (player != null) {
			player.Interactable = this;
			EnterArea (player.gameObject);
			Announcer.Instance.Log ("Klik op E om met " + _text);
		}
	}
	public void OnTriggerExit (Collider other) {
		Player player = other.GetComponent<Player> ();
		if (player != null) {
			player.Interactable = null;
			LeaveArea (player.gameObject);
		}
	}
	public virtual void Interact (GameObject player) { }
	public virtual void EnterArea (GameObject player) { }
	public virtual void LeaveArea (GameObject player) { }
}