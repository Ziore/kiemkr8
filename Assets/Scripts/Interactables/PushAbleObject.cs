﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider))]
public class PushAbleObject : Interactable {
	private Rigidbody _rb;
	void Start () { }

	public override void Interact (GameObject player) {
		player.GetComponent<Animator> ().SetBool ("Pushing", true);
		_rb = player.GetComponent<Rigidbody> ();
		_rb.mass = 10;
	}

	public override void LeaveArea (GameObject player) {
		player.GetComponent<Animator> ().SetBool ("Pushing", false);
		if (_rb != null)
			_rb.mass = 1;
	}
}