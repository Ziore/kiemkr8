using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LeverAnimation : Lever {
    [SerializeField] private Animator _animObj;

    protected override void OnInteract () {
        _animObj.enabled = true;
    }
}