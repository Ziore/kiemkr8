﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Tasks : MonoBehaviour {
    private Image[] _completedImages = new Image[0];
    private int _current = 0;
    private static Tasks _tasks;
    public static Tasks Instance {
        get {
            if (_tasks == null) _tasks = FindObjectOfType<Tasks> ();
            return _tasks;
        }
    }

    void Awake () {
        _completedImages = transform.GetComponentsInChildren<Image> ();
        foreach (Image img in _completedImages) {
            img.enabled = false;
        }
    }

    public void OnCompleteTask () {
        if (_current >= _completedImages.Length) return;
        _completedImages[_current].enabled = true;
        _current++;
    }
}