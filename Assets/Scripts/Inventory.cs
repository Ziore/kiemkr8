﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    private InventorySlot[] _slots = new InventorySlot[5];

    private static Inventory _inventory;
    public static Inventory Instance {
        get {
            if (_inventory == null) _inventory = FindObjectOfType<Inventory> ();
            return _inventory;
        }
    }

    void Start () {
        _slots = GetComponentsInChildren<InventorySlot> ();
        foreach (InventorySlot slot in _slots) {
            slot.SetItem (null);
        }
    }

    public void AddItem (Item item) {
        foreach (InventorySlot slot in _slots) {
            if (slot.Item == null) {
                slot.SetItem (item);
                TryEquipItem (item);
                return;
            }
        }
    }

    public void TryEquipItem (Item item) {
        if (item.itemType == ItemType.Weapon) {
            FindObjectOfType<Player>().EquipWeapon(item.sprite);
        }
    }

    public bool HasItem (Item item) {
        foreach (InventorySlot slot in _slots) {
            if (slot.Item == item) {
                return true;
            }
        }

        return false;
    }
}