using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class LootableItem : MonoBehaviour {
    [SerializeField] private Item _item;

    void Awake () {
        GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;
    }

    void OnTriggerEnter (Collider other) {
        Player p = other.GetComponent<Player> ();
        if (p != null) {
            p.Lootable = this;
        }
    }

    void OnTriggerExit (Collider other) {
        Player p = other.GetComponent<Player> ();
        if (p != null) {
            p.Lootable = null;
        }
    }

    public void LootItem () {
        Inventory.Instance.AddItem (_item);
    }
}