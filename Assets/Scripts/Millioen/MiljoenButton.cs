using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiljoenButton : MonoBehaviour {
    private Button _btn;

    public void Init (float val, MiljoenManager manager) {
        _btn = GetComponent<Button> ();

        _btn.onClick.AddListener (() => manager.SetChosenValue (val, transform.parent.gameObject));
    }
}