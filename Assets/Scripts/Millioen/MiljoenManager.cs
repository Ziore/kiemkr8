﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiljoenManager : MonoBehaviour {
    public List<float> Values = new List<float> ();
    private List<float> _remainingValues = new List<float> ();
    private List<float> _removedValues = new List<float> ();
    private Button[] _btns = new Button[0];
    private Animator _anim;
    [SerializeField] private Transform _btnParent;
    [SerializeField] private Button _rodeKnop;
    [SerializeField] private Button _nextKnop;
    [SerializeField] private Text _valuesLowText;
    [SerializeField] private Text _valuesHighText;
    private int _startValue = 6, _curValue = 6;
    private float _currentMedian;
    private bool _animDone = true;

    void Awake () {
        _anim = GetComponent<Animator> ();
        _btns = _btnParent.GetComponentsInChildren<Button> ();
        _remainingValues = new List<float> (Values);
        UpdateValues ();
        Values.Shuffle ();

        int val = 0;
        for (int i = 0; i < 5; i++) {
            for (int x = 0; x < 5; x++) {
                _btns[val].transform.parent.position = new Vector3 (x * 5, i * 2, i * 4);
                _btns[val].gameObject.AddComponent<MiljoenButton> ().Init (Values[val], this);
                val++;
            }
        }
        _btnParent.transform.position = new Vector3 (-10, 2, 32);
        _rodeKnop.onClick.AddListener (() => AcceptMedian ());
        _nextKnop.onClick.AddListener (() => Next ());

        _rodeKnop.gameObject.SetActive (false);
        _nextKnop.gameObject.SetActive (false);
    }

    private void CinematicAnim () {
        _animDone = false;
    }
    public void CinematicFinished () {
        _animDone = true;
    }

    public void SetChosenValue (float i, GameObject obj) {
        if (_curValue <= 0 || !_animDone) return;

        Announcer.Instance.Log ("Removed : " + i);
        RemoveValue (i);
        obj.SetActive (false);
        CinematicAnim ();
    }

    private void RemoveValue (float i) {
        _removedValues.Add (i);

        _curValue--;
        if (_curValue <= 0) {
            ShowMedian ();
            _nextKnop.gameObject.SetActive (true);
            _rodeKnop.gameObject.SetActive (true);
        }

        UpdateValues ();
    }

    private void ShowMedian () {
        _currentMedian = _remainingValues.Sum () / _remainingValues.Count;
        Announcer.Instance.Log ("value = " + _currentMedian);
    }

    private void Next () {
        _startValue--;
        _curValue = _startValue;
        _rodeKnop.gameObject.SetActive (false);
        _nextKnop.gameObject.SetActive (false);
    }
    private void AcceptMedian () {
        Announcer.Instance.Log ("You have won " + _currentMedian + "!");
    }
    private void UpdateValues () {
        _valuesLowText.text = "";
        _valuesHighText.text = "";

        foreach (float f in _remainingValues) {
            if (f < 10000) {
                if (_removedValues.Contains (f)) {
                    _valuesLowText.text += "\n";
                    continue;
                }

                _valuesLowText.text += f + "\n";
                continue;
            }

            if (_removedValues.Contains (f)) {
                _valuesHighText.text += "\n";
                continue;
            }

            _valuesHighText.text += f + "\n";
        }
    }
}