using System.Collections.Generic;
using UnityEngine;
public static class Statics {

    public static List<T> Shuffle<T> (this List<T> list) {
        for (var i = 0; i < list.Count; i++)
            Swap (list, i, Random.Range (i, list.Count));
        return list;
    }

    public static void Swap<T> (this List<T> list, int i, int j) {
        var temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }
    public static float Sum (this List<float> list) {
        float sum = 0;
        foreach (float f in list) {
            sum += f;
        }
        return sum;
    }
    
    public static bool GetTurn (float x) {
        if (x > 0.1f) {
            return true;
        } else if (x < -0.1f) {
            return false;
        }
        return true;
    }

    public static int ConvertToEven (int a) {
        int Result = Mathf.RoundToInt (Mathf.Ceil ((float) a / 2) * 2);
        return Result;
    }
    public static Vector3 GetAttackDir (Vector3 charpos) {
        Vector3 dir = Statics.GetMouseDir (charpos);
        if (dir == Vector3.zero) {
            dir = new Vector3 (1, 0);
        }
        return dir.normalized;
    }
    public static Vector3 GetMouseDir (Vector3 charPos) {
        Vector3 pos = Input.mousePosition; // use new input system.
        pos.z = 10;
        Vector3 mouseDir = Camera.main.ScreenToWorldPoint (pos) - charPos;
        return mouseDir;
    }
}